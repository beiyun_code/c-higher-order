#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include <thread>
#include <iostream>
using namespace std;
//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	/*for (auto& e : arr)
//	{
//		cout << e << " ";
//	}*/
//	/*for (int i = 0; i <= sizeof(arr); i++)
//	{
//		cout << arr[i] << " ";
//	}*/
//	/*cout << endl;*/
//	/*int* p = nullptr;
//	*p = 10;
//	exit(3);
//	cout << *p << endl;*/
//	abort();
//	return 0;
//}
//#include <stdio.h>
//#include <assert.h>
//
//int main() {
//    int x = -1;
//    // 断言 x 应该是正数，但这里 x 是负数，所以断言会失败
//    assert(x > 0); // 断言失败，程序将打印错误消息并终止
//    return 0;
//}
//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	int len, time;
//	cin >> len >> time;
//	try 
//	{
//		cout << Division(len, time) << endl;
//	}
//
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	
//}
//int main()
//{
//	try
//	{
//		Func(); 
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	catch (...) 
//	{
//			cout << "unkown exception" << endl;
//	}
//	cout << "捕获 Division(len, time) 异常 成功" << endl;
//	return 0;
//}
//异常信息类
//class Exception
//{
//public:
//	Exception(int errid, const string& errmsg)
//		:_errid(errid)
//		,_errmsg(errmsg)
//	{}
//	int GetErrid() const
//	{
//		return _errid;
//	}
//	const string& GetErrmsg() const
//	{
//		return _errmsg;
//	}
//private:
//	int _errid; //错误码
//	string _errmsg; //错误信息
//};
//double Division(int a, int b)
//{
//	 当b == 0时抛出异常
//	if (b == 0)
//		throw Exception(1, "除0错误");
//		throw 1;
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	int len, time;
//	cin >> len >> time;
//	try 
//	{
//		cout << Division(len, time) << endl;
//	}
//
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	
//}
//int main()
//{
//	try
//	{
//		Func(); 
//	}
//	catch (const Exception&e)
//	{
//		cout << "错误码：" <<e.GetErrid()  << endl;
//		cout << "错误信息：" << e.GetErrmsg() << endl;
//	}
//	catch (...) 
//	{
//			cout << "未知异常" << endl;
//	}
//	cout << "捕获 Division(len, time) 异常 成功" << endl;
//	return 0;
//}
//////////////////////////////////////////////////////////////////////////////
//double Division(int a, int b)
//{
//	 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	int* arr = new int[10];
//	int len, time;
//	cin >> len >> time;
//	try 
//	{
//		cout << Division(len, time) << endl;
//	}
//
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	delete[] arr;
//}
//int main()
//{
//	try
//	{
//		Func(); 
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	catch (...) 
//	{
//			cout << "unkown exception" << endl;
//	}
//	cout << "捕获 Division(len, time) 异常 成功" << endl;
//	return 0;
//}

//
//double Division(int a, int b) noexcept
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//	{
//		throw "Division by zero condition!";
//	}
//	return (double)a / (double)b;
//}
//void Func()
//{
//	// 这里可以看到如果发生除0错误抛出异常，另外下面的array没有得到释放。
//	// 所以这里捕获异常后并不处理异常，异常还是交给外面处理，这里捕获了再
//	// 重新抛出去。
//	int* array = new int[10];
//	try {
//		int len, time;
//		cin >> len >> time;
//		cout << Division(len, time) << endl;
//	}
//	catch (...)
//	{
//		cout << "delete []" << array << endl;
//		delete[] array;
//		throw;
//	}
//	// ...
//	cout << "delete []" << array << endl;
//	delete[] array;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	return 0;
//}
class Exception
{
public:
	Exception(int errid, const string& msg)
		:_errid(errid)
		, _errmsg(msg)
	{}

	virtual string what() const
	{
		return _errmsg;
	}

	int GetErrid() const
	{
		return _errid;
	}

protected:
	int _errid;     // 错误码
	string _errmsg; // 错误描述
};

class SqlException : public Exception
{
public:
	SqlException(int errid, const string& msg, const string& sql)
		:Exception(errid, msg)
		, _sql(sql)
	{}

	virtual string what() const
	{
		string msg = "SqlException：";
		msg += _errmsg;
		msg += "->";
		msg += _sql;

		return msg;
	}

protected:
	string _sql;
};

class CacheException : public Exception
{
public:
	CacheException(const string& errmsg, int id)
		:Exception(id, errmsg)
	{}

	virtual string what() const
	{
		string msg = "CacheException：";
		msg += _errmsg;

		return msg;
	}
};

class HttpServerException : public Exception
{
public:
	HttpServerException(const string& errmsg, int id, const string& type)
		:Exception(id, errmsg)
		, _type(type)
	{}

	virtual string what() const
	{
		string msg = "HttpServerException：";
		msg += _errmsg;
		msg += "->";
		msg += _type;

		return msg;
	}

private:
	const string _type;
};


void SQLMgr()
{
	srand(time(0));
	if (rand() % 7 == 0)
	{
		throw SqlException(100, "权限不足", "select * from name = '张三'");
	}

	cout << "调用成功" << endl;
}

void CacheMgr()
{
	srand(time(0));
	if (rand() % 5 == 0)
	{
		throw CacheException("权限不足", 100);
	}
	else if (rand() % 6 == 0)
	{
		throw CacheException("数据不存在", 101);
	}

	SQLMgr();
}

void HttpServer()
{
	// 模拟服务出错
	srand(time(0));
	if (rand() % 3 == 0)
	{
		throw HttpServerException("请求资源不存在", 100, "get");
	}
	else if (rand() % 4 == 0)
	{
		throw HttpServerException("权限不足", 101, "post");
	}

	CacheMgr();
}
int main()
{
	while (1)
	{
		this_thread::sleep_for(chrono::seconds(1));

		try
		{
			HttpServer();
		}
		catch (const Exception& e) // 这里捕获父类对象就可以
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (...)
		{
			cout << "Unkown Exception" << endl;
		}
	}

	return 0;
}