#define _CRT_SECURE_NO_WARNINGS 1

#include "String.h"

//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p)
// 
//	:_name(p._name)
//	,_age(p._age)
//	{}
//	Person(Person&& p) = default;
//	Person& operator=(Person&& p) = default;
//	
//	Person& operator=(const Person& p)
//	{
//	if(this != &p)
//	{
//	_name = p._name;
//	_age = p._age;
//	}
//	return *this;
//	}
//	~Person()
//	{}
//
//private:
//	gx::string _name;
//	int _age;
//};
//int main()
//{
//	Person s1;
//	Person s2 = s1;
//	Person s3 = move(s1);
//	Person s4;
//	s4 = move(s2);
//	return 0;
//}
//
//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p) = delete;
//private:
//	gx::string _name;
//	int _age;
//};
//int main()
//{
//	Person s1;
//	Person s2 = s1;
//	Person s3 = move(s1);
//	return 0;
//}
//int main()
//{
//	/*bit::string s1;
//	s1 = bit::to_string(1234);*/
//
//	gx::string("hello world");
//	//const gx::string& ref1 = gx::string("hello world");
//	//gx::string s2;
//
//	const gx::string& ref2 = gx::to_string(1234);
//	cout << "111111111111111111" << endl;
//
//	/*const bit::string& ref2 = bit::to_string(1234);
//	cout << "111111111111111111" << endl;*/
//
//	return 0;
//}

//class A
//{
//public:
//	void print()
//	{
//		std::cout << _a << std::endl;
//	}
//private:
//	int _a = 1;
//};
//
//int main()
//{
//	A a;
//	a.print();
//	
//}
//class Person
//{
//public:
//	Person(const char* name, int age)
//		:_name(name)
//		, _age(age)
//	{}
//
//	
//	Person(const char* name)
//		:Person(name, 18) // 委托构造
//	{}
//
//private:
//	gx::string _name; // 自定义类型
//	int _age = 1;		   // 内置类型
//};
//
//int main()
//{
//	Person s1("张三");
//
//	return 0;
//}
//template<class ...Args>
//void ShowList(Args...args)
//{
//	cout << sizeof...(args) << endl;
//}
//int main()
//{
//	ShowList(1, 'x');
//}
////////////////////////////////////////
//void _ShowList()
//{
//	cout << endl;
//}
//template <class T ,class ...Args>
//void ShowList(const T& val, Args ...args)
//{
//	cout << __FUNCTION__ << "(" << sizeof...(args) << ")" << endl;
//	cout << val << endl;
//	ShowList(args...); //语法规定...必须在后面
//}
//int main()
//{
//	ShowList(1, 'A', std::string("sort"));
//	return 0;
//}
//template <class T, class ...Args>
//void _ShowList(const T& val, Args... args)
//{
//	cout << __FUNCTION__ << "(" << sizeof...(args) << ")" << endl;
//
//	cout << val << endl;
//	_ShowList(args...);
//}
//template <class ...Args>
//void ShowList(Args... args)
//{
//	_ShowList(args...);
//}
//
//int main()
//{
//	ShowList(1, 'A', std::string("sort"));
//
//	return 0;
//}

//
//template <class T>
//int PrintArg(T t)
//{
//	cout << t << " ";
//	return 0;
//}
////展开函数
//template <class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { PrintArg(args)... };
//	cout << endl;
//}
//int main()
//{
//	ShowList(1);
//	ShowList(1, 'A');
//	ShowList(1, 'A', std::string("sort"));
//	return 0;
//}

///////////////////////////////////
//#include "Date.h"
//int main()
//{
//	/*list<gx::string> lt;
//	gx::string s1("1111");
//	lt.push_back(s1);
//	lt.emplace_back(s1);*/
//	/*list<Date> lt2;
//	Date d1(2024, 5, 20);
//	Date d2(2024, 5, 21);*/
//	//lt2.push_back(d1);
//	//lt2.emplace_back(d2);
//	/*lt2.push_back(move(d1));
//	lt2.emplace_back(move(d2));*/
//	// 有区别
//	//cout << "=========================" << endl;
//	//lt2.push_back(Date(2023, 5, 28));
//	//lt2.push_back({ 2023, 5, 28 });
//
//	//cout << endl;
//	//lt2.emplace_back(Date(2023, 5, 28)); // 构造+移动构造
//	//lt2.emplace_back(2023, 5, 28);
//	list<gx::string> lt;
//	lt.emplace_back("hello world!");
//
//	return 0;
//
//}

#include <algorithm>
#include <functional>
//int main()
//{
//	int array[] = { 4,1,8,5,3,7,0,9,2,6 };
//	// 默认按照小于比较，排出来结果是升序
//	std::sort(array, array + sizeof(array) / sizeof(array[0]));
//	// 如果需要降序，需要改变元素的比较规则
//	std::sort(array, array + sizeof(array) / sizeof(array[0]), greater<int>());
//	return 0;
//}
struct Goods
{
	string _name;  // 名字
	double _price; // 价格
	int _evaluate; // 评价
	Goods(const char* str, double price, int evaluate)
		:_name(str)
		, _price(price)
		, _evaluate(evaluate)
	{}
};
//struct ComparePriceLess
//{
//	bool operator()(const Goods& gl, const Goods& gr)
//	{
//		{
//			return gl._price < gr._price;
//		
//		}	
//	}
//};
//struct ComparePriceGreater
//{
//	bool operator()(const Goods& gl, const Goods& gr)
//	{
//		return gl._price > gr._price;
//	}
//};
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,
//   3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), ComparePriceLess());
//	
//	sort(v.begin(), v.end(), ComparePriceGreater());
//}
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,
//   3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._price < g2._price; });
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._price > g2._price; });
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._evaluate < g2._evaluate; });
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._evaluate > g2._evaluate; });
//}
//int main()
//{
//
//
//	//auto add1 = [](int x, int y)->int 
//	//{
//	//	return x + y; 
//	//};
//	//cout << add1(1, 2) << endl;
//
//	////最简单的lambda表达式 该lambda表达式没有任何意义
//	//[] {};
//
//	int x = 1, y = 0;
	/*auto swap1 = [](int& rx, int& ry)
	{
		int tmp = rx;
		rx = ry;
		ry = tmp;
	};
	//传值捕捉
	auto swap1 = [x,y]() mutable
	{
		int tmp = x;
		x = y;
		y = tmp;
	};*/
	//引用捕捉
//	auto swap2 = [&]() 
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	swap2();
//	cout << x << " " << y << endl;
//	 //混合捕捉
//	auto func1 = [&x, y]()
//	{
//		//...
//	};
//
//
//
//	 //全部传值捕捉
//	auto func3 = [=]()
//	{
//		//...
//	};
//
//	 //全部引用捕捉，x传值捕捉
//	auto func4 = [&, x]()
//	{
//		//...
//	};
////
//	return 0;
//}

