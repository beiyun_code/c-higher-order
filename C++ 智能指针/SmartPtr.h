﻿#pragma once
#include <mutex>
#include <atomic>
#include <iostream>
#include <functional>
using namespace std;
namespace gx
{
	template<class T>
	class SmartPtr
	{
	public:
		SmartPtr(T* ptr)
			:_ptr(ptr)
		{}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
		~SmartPtr()
		{
			if (_ptr)
			{
				cout << "~SmartPtr():delete" << _ptr << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;
	};
	template<class T>
	class auto_ptr
	{
	public:
		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
		auto_ptr(auto_ptr<T>& ap)
			:_ptr(ap._ptr)
		{
			ap._ptr = nullptr;
		}
		~auto_ptr()
		{
			if (_ptr)
			{
				cout << "delete" << _ptr << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;
	};
	template<class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
		unique_ptr(unique_ptr<T>& up) = delete;
		unique_ptr<T>& operator=(const unique_ptr<T>& up) = delete;

		~unique_ptr()
		{
			if (_ptr)
			{
				cout << "delete" << _ptr << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;
	};
	void test_auto()
	{
		auto_ptr<int> p1(new int(1));
		auto_ptr<int> p2(p1);
		*p1 = 10;
		*p2 = 20;
	}
	void test_unique()
	{
		unique_ptr<int> p1(new int(1));
		//unique_ptr<int> p2(p1);
	}


	//template<class T>
	//class shared_ptr
	//{
	//public:
	//	shared_ptr(T* ptr = nullptr)
	//		: _ptr(ptr), _pcount(new int(1)), _pmtx(new mutex) 
	//	{}

	//	~shared_ptr()
	//	{
	//		Release();
	//	}

	//	void Release()
	//	{
	//		
	//		int flag = false;
	//		if (--(*_pcount) == 0)
	//		{
	//			cout << "delete:" << _ptr << endl;
	//			delete _ptr;
	//			delete _pcount;
	//			unique_lock<mutex> lck(*_pmtx);
	//			flag = true;
	//		}
	//		if (flag)
	//			delete _pmtx;
	//	}

	//	void AddCount()
	//	{
	//		unique_lock<mutex> lck(*_pmtx);
	//		++(*_pcount);
	//	}

	//	shared_ptr(const shared_ptr<T>& sp)
	//		:_ptr(sp._ptr)
	//		, _pcount(sp._pcount)
	//		, _pmtx(sp._pmtx)
	//	{
	//		AddCount();
	//	}
	//	shared_ptr<T>& operator=(const shared_ptr<T>& sp)
	//	{
	//		if (_ptr != sp._ptr)
	//		{
	//			Release();

	//			_ptr = sp._ptr;
	//			_pcount = sp._pcount;
	//			_pmtx = sp._pmtx;

	//			AddCount();
	//		}

	//		return *this;
	//	}

	//	T& operator*()
	//	{
	//		return *_ptr;
	//	}

	//	T* operator->()
	//	{
	//		return _ptr;
	//	}

	//	T* get()
	//	{
	//		return _ptr;
	//	}

	//	int use_count()
	//	{
	//		return *_pcount;
	//	}
	//private:
	//	T* _ptr;          // 指向管理对象的指针
	//	int* _pcount;     // 引用计数
	//	mutex* _pmtx; // 互斥锁，用于同步对引用计数的访问
	//};
	// ///////////////////////////////////////////////////////////////////
	//template<class T>
	//class shared_ptr
	//{
	//public:
	//	shared_ptr(T* ptr)
	//		: _ptr(ptr)
	//		, _pcount(new std::atomic<int>(1)) 
	//	{}
	//	// 复制构造函数
	//	shared_ptr(const shared_ptr<T>& sp)
	//		: _ptr(sp._ptr)
	//		, _pcount(sp._pcount) 
	//	{
	//		SubAdd();
	//	}
	//	// 赋值操作符
	//	shared_ptr<T>& operator=(const shared_ptr<T>& sp) 
	//	{
	//		if (this != &sp) 
	//		{
	//			// 先递减当前对象的引用计数
	//			if (_pcount->fetch_sub(1, std::memory_order_acq_rel) == 1)
	//			{
	//				delete _ptr;
	//				delete _pcount;
	//			}
	//			// 然后复制新对象的指针和引用计数
	//			_ptr = sp._ptr;
	//			_pcount = sp._pcount;
	//			// 递增新对象的引用计数
	//			SubAdd();
	//		}
	//		return *this;
	//	}
	//	void SubAdd()
	//	{	// 自动递增引用计数
	//		_pcount->fetch_add(1, std::memory_order_relaxed);
	//	}
	//	// 解引用操作符
	//	T& operator*() { return *_ptr; }
	//	// 成员访问操作符
	//	T* operator->() { return _ptr; }

	//	// 析构函数
	//	~shared_ptr() 
	//	{
	//		if (_pcount->fetch_sub(1, std::memory_order_acq_rel) == 1)
	//		{
	//			delete _ptr;
	//			delete _pcount;
	//		}
	//	}

	//	T* get()
	//	{
	//		return _ptr;
	//	}

	//	int use_count()
	//	{
	//		return *_pcount;
	//	}
	//private:
	//	T* _ptr;
	//	std::atomic<int>* _pcount;
	//};
template<class T>
class shared_ptr
{
public:
	shared_ptr(T* ptr = nullptr)
		:_ptr(ptr)
		, _pcount(new int(1))
		, _pmtx(new mutex)
	{}
	template<class D>
	shared_ptr(T* ptr, D del)
		: _ptr(ptr)
		, _pcount(new int(1))
		, _pmtx(new mutex)
		, _del(del)
	{}
	~shared_ptr()
	{
		Release();
	}

	void Release()
	{
		_pmtx->lock();
		int flag = false;
		if (--(*_pcount) == 0)
		{
			//cout << "delete:" << _ptr << endl;
			//delete _ptr;
			_del(_ptr);
			delete _pcount;
			flag = true;
		}
		_pmtx->unlock();
		if (flag)
			delete _pmtx;
		
	}

	void AddCount()
	{
		_pmtx->lock();

		++(*_pcount);

		_pmtx->unlock();
	}

	shared_ptr(const shared_ptr<T>& sp)
		:_ptr(sp._ptr)
		, _pcount(sp._pcount)
		, _pmtx(sp._pmtx)
	{
		AddCount();
	}

	// sp1 = sp4
	// sp1 = sp1;
	// sp1 = sp2;
	shared_ptr<T>& operator=(const shared_ptr<T>& sp)
	{
		if (_ptr != sp._ptr)
		{
			Release();

			_ptr = sp._ptr;
			_pcount = sp._pcount;
			_pmtx = sp._pmtx;

			AddCount();
		}

		return *this;
	}

	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}

	T* get() const
	{
		return _ptr;
	}
	T* get() 
	{
		return _ptr;
	}

	int use_count()
	{
		return *_pcount;
	}

private:
	T* _ptr;
	int* _pcount;
	mutex* _pmtx;
	
	 //包装器
	function<void(T*)> _del = [](T* ptr) {
		cout << "lambda delete:" << ptr << endl;
		delete ptr;
	};
};


template<class T>
class weak_ptr
{
public:
	weak_ptr()
		:_ptr(nullptr)
	{}

	weak_ptr(const shared_ptr<T>& sp)
		:_ptr(sp.get())
	{}

	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}

	T* get()
	{
		return _ptr;
	}

private:
	T* _ptr;
};



	struct Date
	{
		int _year = 0;
		int _month = 0;
		int _day = 0;
		~Date() {};
	};

	void SharePtrFunc(gx:: shared_ptr<Date>& sp, size_t n, mutex& mtx)
	{
		//cout << sp.get() << endl;

		for (size_t i = 0; i < n; ++i)
		{	
			// 这里智能指针拷贝会++计数，智能指针析构会--计数，这里是线程安全的。
			gx::shared_ptr<Date> copy(sp);
			lock_guard<std::mutex> guard(mtx);
			sp->_year++;
			sp->_month++;
			sp->_day++;

		}
	}

	void test_shared_safe()
	{
		gx::shared_ptr<Date> p(new Date);
		cout << p.get() << endl;
		const size_t n = 500000;
		mutex mtx;
		thread t1(SharePtrFunc, ref(p), n, ref(mtx));
		thread t2([&](){
			SharePtrFunc(ref(p), n, ref(mtx)); // 使用get()来获取原始指针
		});
		t1.join();
		t2.join();

		cout << p.use_count() << endl;
		cout << p->_year << endl;
		cout << p->_month << endl;
		cout << p->_day << endl;
	}
	struct ListNode
	{
		gx::weak_ptr<ListNode> _next;
		gx::weak_ptr<ListNode> _prev;
		int _val;
		~ListNode()
		{
			cout << "~ListNode()" << endl;
		}
	};
	template<class T>
	struct DeleteArry
	{
		void operator()(T* ptr)
		{
			cout << "void operator(T* ptr)" << endl;
			delete[] ptr;
		}
	};
	//循环引用
	void test_shared_cycle()
	{
		/*ListNode* n1 = new ListNode;
		ListNode* n2 = new ListNode;*/
		gx::shared_ptr<ListNode> n1 (new ListNode);
		gx::shared_ptr<ListNode> n2 (new ListNode);
		//没有链接之前的引用计数
		cout << n1.use_count() << endl;
		cout << n2.use_count() << endl;
		n1->_next = n2;
		n2->_prev = n1;
		//链接之后的引用计数
		cout << n1.use_count() << endl;
		cout << n2.use_count() << endl;
	}
	void test_shared_delete()
	{	//仿函数
		std::shared_ptr<Date> spd0(new Date[10],DeleteArry<Date>());
		//lambda
		std::shared_ptr<Date> spd1(new Date[10], [](Date* ptr) 
			{	cout << "Lambda delete[]" << endl;
				delete[] ptr;
			}
		);
		//文件指针
		std::shared_ptr<FILE> spd2(fopen("Test.cpp","r"), [](FILE* ptr)
			{	cout << "Lambda fclose:" << endl;
				fclose(ptr);
			}
		);
	}
	void test_shared_delete1()
	{	//仿函数
		gx::shared_ptr<Date> spd0(new Date[10], DeleteArry<Date>());
		//lambda
		gx::shared_ptr<Date> spd1(new Date[10], [](Date* ptr)
			{	cout << "Lambda delete[]" << endl;
		delete[] ptr;
			}
		);
		//文件指针
		gx::shared_ptr<FILE> spd2(fopen("Test.cpp", "r"), [](FILE* ptr)
			{	cout << "Lambda fclose:" << endl;
		fclose(ptr);
			}
		);
	}
}