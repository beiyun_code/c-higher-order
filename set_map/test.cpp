#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <map>
#include <set>


using namespace std;

void set_test()
{
	set<string> s;
	s.insert("张三");
	s.insert("李四");
	s.insert("王二麻子");
	s.insert("赵六");
	s.insert("孙七");
	set<string>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
	auto it1 = s.begin();
	while (it1 != s.end())
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;
}
void set_test2()
{
	//排序+去重
	set<int> s1;
	s1.insert(3);
	s1.insert(1);
	s1.insert(4);
	s1.insert(2);
	s1.insert(1);
	s1.insert(2);
	auto it1 = s1.begin();
	while (it1 != s1.end())
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;
}
void map_test1()
{
	map<string, int> dict;
	dict.insert(make_pair("one", 1));
	dict.insert(make_pair("two", 2));
	dict.insert(make_pair("three", 3));
	dict.insert(make_pair("four", 4));
	//插入也可以用方括号
	dict["five"] = 5;
	dict["six"] = 6;
	dict["six"] = 6; //插入失败

	map<string, int>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first << ":" << it->second << " ";
		++it;
	}
	cout << endl;

}
void map_test2()
{
	map<string, int> dict;
	dict.insert(make_pair("one", 1));
	dict.insert(make_pair("two", 2));
	dict.insert(make_pair("three", 3));
	dict.insert(make_pair("four", 4));
	//插入也可以用方括号
	dict["five"] = 5;
	dict["six"] = 6;
	dict["six"] = 6; //插入失败

	map<string, int>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first << ":" << it->second << " ";
		++it;
	}
	cout << endl;

}
void map_test3()
{
	string arr[] = { "西瓜", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉", "梨" };
	map<string, int> countMap;
	//传统写法插入
	for (auto& e : arr)
	{
		auto ret = countMap.find(e);
		if (ret == countMap.end())
		{
			countMap.insert(make_pair(e, 1));
		}
		else
		{
			ret->second++;
		}
	}
	for (auto &kv : countMap)
	{
		cout << kv.first << ": " << kv.second << endl;
	}
	// 【】写法
	/*for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}*/
}
int main()
{
	//set_test();
	map_test3();

	return 0;
}