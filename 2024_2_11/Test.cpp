#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
using namespace std;
//class person
//{
//protected:
//	string name = "张三";
//	int age = 18;
//	double hight = 0.0;
//	vector <int> id;
//
//};
//class Student : public person
//{
//protected:
//	int _stuid = 1; // 学号
//};

//class Person
//	{
//	public:
//		void Print()
//		{
//			cout << "name:" << _name << endl;
//			cout << "age:" << _age << endl;
//		}
//	//protected:   // 在子类可见的(不能用) 只能防外面
//	private:  // 在子类是不可见(不能用)  不仅仅可以放外人还可以防儿子
//		string _name = "peter"; // 姓名
//		int _age = 18;  // 年龄
//	};
//	
//	class Student : public Person
//	{
//	public:
//		void func()
//		{
//			// 不可见
//			// cout <<_name << endl;
//			cout << "void func()" << endl;
//		}
//	protected:
//		int _stuid; // 学号
//	};
//	
//	class Teacher : public Person
//	{
//	protected:
//		int _jobid; // 工号
//	};
//	
//int main()
//{
//	Student s;
//	Person p = s;
//	Person& rp = s;
//	Person* ptrp = &s;
//	double d = 1.1;
//	int i = d;
//	
//
//	return 0;
//}
/////////////////////////////////////////////////////////////////
//class Person
//{
//protected:
//	string _name = "小李子";  // 姓名
//	int _num = 111; //身份证号码
//};
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << " 姓名:" << _name << endl;
//		cout << " 身份证号:" << Person::_num << endl;
//		cout << " 学号:" << _num << endl;
//	}
//protected:
//	int _num = 999; // 学号
//};
//void Test()
//{
//	Student s1;
//	s1.Print();
//};
//class Person
//{
//public:
//	Person(const char* name = "peter")
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		
//			cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		: Person(name)
//		, _num(num)
//	{
//		cout << "Student()" << endl;
//	}
//
//	Student(const Student& s)
//		: Person(s)
//		, _num(s._num)
//	{
//		cout << "Student(const Student& s)" << endl;
//	}
//
//	Student& operator = (const Student& s)
//	{
//		cout << "Student& operator= (const Student& s)" << endl;
//		if (this != &s)
//		{
//			Person::operator =(s);
//			_num = s._num;
//		}
//		return *this;
//	}
//
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//protected:
//	int _num; //学号
//};
//void Test()
//{
//	Person p;
//	Student s1("jack", 18);
//	/*Student s2(s1);
//	p = s1;*/
//}

//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//	friend void Display(const Person& p, const Student& s);
//protected:
//	int _stuNum; // 学号
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}
//class Person
//{
//public:
//	Person() { ++_count; }
//protected:
//	string _name; // 姓名
//public:
//	static int _count; // 统计人的个数。
//};
//int Person::_count = 0;
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//class Graduate : public Student
//
//{
//protected:
// string _seminarCourse; // 研究科目
//};
//void TestPerson()
//{
//	Student s1;
//	Student s2;
//	Student s3;
//	Graduate s4;
//	cout << " 人数 :" << Person::_count << endl;
//	Student::_count = 0;
//	cout << " 人数 :" << Person::_count << endl;
//}
//void main()
//{
//	TestPerson();
//}

//int main()
//{
//	for (int i = 1; i < 10; i++)
//	{
//		for (int j = 1; j <=i; j++)
//		{
//			printf("%d * %d = %-2d", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
