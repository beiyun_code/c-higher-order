#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;

//class Person
//{
//public:
//	string _name;
//};
//class Student :virtual public Person
//{
//protected:
//	int _num;
//};
//class Teacher : virtual public	Person
//{
//protected:
//	int _id;
//
//};
//class Assistant :virtual public Person
//{
//protected:
//	string _majorCouse;
//};
//
//class A
//{
//public:
//	int _a;
//};
//class B:virtual public A
//{
//public:
//	int _b;
//};
//class C:virtual  public A
//{
//public:
//	int _c;
//};
//class D: public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}
//class Person
//{
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student:public Person
//{
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//
//};
//void Func(Person* p)
//{
//	p->BuyTicket();
//	delete p;
//}
//int main()
//{
//	Func(new Person);
//	Func(new Student);
//	return 0;
//}
//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};
//
//class Car {
//public:
//	 void Drive() {}
//};
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};
//int main()
//{
//	return 0;
//}
//class Person {
//public:
//	virtual ~Person() { cout << "~Person()" << endl; }
//};
//class Student : public Person {
//public:
//	virtual ~Student() { cout << "~Student()" << endl; }
//};
//// 只有派生类Student的析构函数重写了Person的析构函数，下面的delete对象调用析构函数，才能构成多态，才能保证p1和p2指向的对象正确的调用析构函数。
//int main()
//{
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//	
//		
//	delete p1;
//	delete p2;
//	return 0;
//}
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	Base b;
//	cout << sizeof(b) << endl;
//	return 0;
//}

//
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//			cout << "Derive::Func1()" << endl;
//	}
//private:
//	int _d = 2;
//};
//int main()
//{
//	Base b;
//	Derive d;
//	return 0;
//}
//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//	virtual void Func() { cout << "买票-全价" << endl; }
//
//	int _a = 0;
//
//};
//
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//
//	int _b = 1;
//};
//
//void Func(Person p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person mike;
//	Func(mike);
//
//	Student johnson;
//	Func(johnson);
//
//	mike = johnson;
//
//	return 0;
//}
typedef void (*VF_PRF)();//函数指针
class Base1
{
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};
class Base2
{
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
		int b2;
};
class Derive : public Base1, public Base2
{
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};
void PrintVFTable(VF_PRF* table)
{
	for (int i = 0; table[i] != nullptr; i++)
	{
		printf("[%d]:%p->", i, table[i]);
		VF_PRF f = table[i];
		f();
	}
	cout << endl;
}
 
//int main()
//{
//
//	Derive d;
//	Base1* b = &d;
//	PrintVFTable(*(VF_PRF**)&d);
//	PrintVFTable(*(VF_PRF**)b);
//
//	return 0;
//}
int main()
{
	Derive d;
	Base1* ptr1 = &d;
	Base2* ptr2 = &d;
	ptr1->func1();
 
	ptr2->func1();
	return 0;
}