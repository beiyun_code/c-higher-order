#define _CRT_SECURE_NO_WARNINGS 1
// to_string example
#include <iostream>   // std::cout
#include <string>     // std::string, std::to_string

int main()
{
	std::string pi = "pi is " + std::to_string('(');
	std::string perfect = std::to_string(1 + 2 + 4 + 7 + 14) + " is a perfect number";
	std::cout << pi << '\n';
	std::cout << perfect << '\n';
	return 0;
}