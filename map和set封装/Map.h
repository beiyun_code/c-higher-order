#pragma once
#include "RBTree.h"

namespace gx
{
	template <class K, class V>
	class map
	{
		struct MapkeyofT
		{
			const K& operator()(const pair<const K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapkeyofT>::iterator iterator;
		
		iterator begin()
		{
			return _t.begin();
		}
		iterator end()
		{
			return _t.end();
		}
		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = _t.Insert(make_pair(key, V()));
			return ret.first->second;
		}
		pair<iterator, bool>  insert(const pair<const K,V>& kv)
		{
			return _t.Insert(kv);
		}
		iterator find(const K& key)
		{
			return _t.Find(key);
		}

	private:
		RBTree<K, pair<const K, V>, MapkeyofT> _t;
	};
	void test_map1()
	{
		int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
		map<int, int> m;
		for (auto e : a)
		{
			m.insert(make_pair(e, e));
		}
		map<int, int>::iterator it = m.begin();
		while (it != m.end())
		{
			cout << it->first << ":" << it->second << endl;
			++it;
		}
		cout << endl;
	}
	void test_map2()
	{
		string arr[] = { "����", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶", "��" };
		map<string, int> mCount;
		for (auto& e : arr)
		{
			
			mCount[e]++;
		}
		for (auto kv : mCount)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
		 
	}
}